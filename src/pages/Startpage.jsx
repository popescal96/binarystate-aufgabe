import * as React from "react";
import { Link } from "react-router-dom";

export default function Startpage() {
  return (
    <div className="start_page">
      <div className="text">Hello and welcome to Binarystate GmbH</div>
      <p>
        In order to navigate to the second page of this website, please click{" "}
        <Link className="fade_in" to="/secondpage">
          here
        </Link>
      </p>
    </div>
  );
}
