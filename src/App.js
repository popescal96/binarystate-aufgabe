import { Route, BrowserRouter, Routes, Switch } from "react-router-dom";
import "./App.css";
import Secondpage from "./pages/Secondpage";
import Startpage from "./pages/Startpage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Startpage />} />
        <Route exact path="/secondpage" element={<Secondpage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
